.PHONY: default
.PHONY: install
.PHONY: uninstall

default:
	@echo "You have to specify install or uninstall."

install:
	cargo check --all
	cargo build --release
	install systemd/new-home-gpio.service /usr/lib/systemd/system/new-home-gpio.service
	install target/release/new-home-gpio /usr/bin/new-home-gpio
	mkdir -p /etc/new-home-gpio/resources
	cp -R resources/* /etc/new-home-gpio/resources

uninstall:
	rm /usr/lib/systemd/system/new-home-gpio.service
	rm /usr/bin/new-home-gpio
	rm -r /etc/new-home-gpio
