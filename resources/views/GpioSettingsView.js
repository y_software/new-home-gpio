import {ApplicationWindow} from '@framework/script/element/ApplicationWindow.js';
import {SmartTitleTag} from '@framework/script/element/SmartTitle.js';
import {SmartFormTag} from '@framework/script/element/form/SmartForm.js';
import {SelectInputTag} from '@framework/script/element/form/SelectInput.js';
import {SmartTextTag} from "@framework/script/element/SmartText.js";

export class GpioSettingsView extends ApplicationWindow {
    output = document.createElement('span');

    /**
     * @returns {Promise<[HTMLElement]>}
     */
    async getWindowComponents() {
        const titleLine = SmartTitleTag`GPIO Channel Generator`;
        titleLine.classList.add('title-line');
        titleLine.classList.add('grid-container--full-width');

        const form = SmartFormTag``;
        form.classList.add('grid-container--full-width')
        form.addInput(SelectInputTag('scheme', ['gpio'])`Scheme`)
        form.addInput(SelectInputTag('pin', this._getPins())`GPIO Pin`)
        form.addInput(SelectInputTag('action', ['on', 'off', 'toggle'])`Action`)

        form.addEventListener('SmartForm.update', this._onUpdateInputs.bind(this));
        this._onUpdateInputs({detail: {getData: form.getData.bind(form)}});

        return [titleLine, form, SmartTextTag`Channel: ${this.output}`];
    }

    /**
     * @returns {[number]}
     *
     * @private
     */
    _getPins() {
        return (new Array(28)).fill(0).map((_val, index) => index);
    }

    /**
     * @param detail
     *
     * @private
     */
    _onUpdateInputs({detail}) {
        const {scheme, pin, action} = detail.getData();
        const channel = `${scheme || 'gpio'}://${pin || '0'}/${action || 'on'}`;

        this.output.innerText = channel;
    }
}

export const GpioSettingsViewTag = GpioSettingsView.register();
