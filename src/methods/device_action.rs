use std::sync::{Arc, RwLock};

use new_home_application::method::method_callable::MethodCallable;
use new_home_application::method::method_structure::{MethodArguments, MethodResult};
use new_home_application::method::response_formatter::{JsonResponseFormatter, ResponseFormatter};
use url::{Host, Url};

use crate::service::gpio_manager::GpioManager;

pub struct DeviceAction {
    gpio_manager: Arc<RwLock<GpioManager>>,
}

impl DeviceAction {
    pub fn new(gpio_manager: Arc<RwLock<GpioManager>>) -> Self {
        Self { gpio_manager }
    }

    fn proto_gpio(&self, url: &Url) -> MethodResult {
        let pin = url.host().unwrap_or(Host::Domain("")).to_string();
        let action = String::from(url.path().trim_matches('/'));

        let pin_number = match pin.parse::<i32>() {
            Err(error) => return JsonResponseFormatter::error(&error.to_string()),
            Ok(number) => number,
        };

        let state = match action.as_str() {
            "toggle" => self.gpio_manager.read().unwrap().gpio_toggle(pin_number),
            "on" => self.gpio_manager.read().unwrap().gpio_on(pin_number),
            "off" => self.gpio_manager.read().unwrap().gpio_off(pin_number),
            _ => return JsonResponseFormatter::error(&format!("Undefined action {}", &action)),
        };

        let mut response = JsonResponseFormatter::new();
        response.add_success(&format!("State is {}", if state { "on" } else { "off" }));

        response.as_result(0)
    }
}

impl MethodCallable for DeviceAction {
    fn call(&self, arguments: MethodArguments) -> MethodResult {
        let channel = arguments.get_argument(&String::from("channel")).unwrap();

        if !channel.is_string() {
            return JsonResponseFormatter::error(&String::from(
                "Channel is not set or is not a string!",
            ));
        }

        let url = match Url::parse(channel.as_str().unwrap()) {
            Err(error) => return JsonResponseFormatter::error(&error.to_string()),
            Ok(url) => url,
        };

        match url.scheme() {
            "gpio" => self.proto_gpio(&url),
            _ => JsonResponseFormatter::error(&format!("Undefined url scheme {}", url.scheme())),
        }
    }
}
