use std::collections::HashMap;

use new_home_application::method::method_callable::MethodCallable;
use new_home_application::method::method_structure::{MethodArguments, MethodResult};
use new_home_application::method::response_formatter::JsonResponseFormatter;

pub struct GetScript {
    scripts: HashMap<String, String>,
}

impl GetScript {
    pub fn new() -> Self {
        let mut instance = Self {
            scripts: Default::default(),
        };

        instance.setup_scripts();

        instance
    }

    fn setup_scripts(&mut self) {
        self.scripts.insert(
            "/views/GpioSettingsView.js".to_string(),
            include_str!("../../resources/views/GpioSettingsView.js").to_string(),
        );
    }
}

impl MethodCallable for GetScript {
    fn call(&self, arguments: MethodArguments) -> MethodResult {
        let script_name = match arguments.get_argument(&"script_name".to_string()) {
            None => {
                return JsonResponseFormatter::error(
                    &"Argument is 'script_name' not set".to_string(),
                );
            }
            Some(value) => match value.as_str() {
                Some(value) => String::from(value),
                None => {
                    return JsonResponseFormatter::error(
                        &"Argument 'script_name' is not of type string".to_string(),
                    );
                }
            },
        };

        return match self.scripts.get(&script_name) {
            None => JsonResponseFormatter::error(&format!(
                "[GPIO Application] Could not find script {}",
                &script_name
            )),
            Some(script) => MethodResult {
                code: 0,
                message: Some(json!({ "script": script })),
            },
        };
    }
}
