use new_home_application::method::method_callable::MethodCallable;
use new_home_application::method::method_structure::{MethodArguments, MethodResult};

pub struct ConfigRoot;

impl ConfigRoot {
    pub fn new() -> Self {
        Self {}
    }
}

impl MethodCallable for ConfigRoot {
    fn call(&self, _arguments: MethodArguments) -> MethodResult {
        MethodResult {
            code: 0,
            message: Some(json!({
                "scripts": ["@app/views/GpioSettingsView.js"],
                "component": "gpio-settings-view"
            })),
        }
    }
}
