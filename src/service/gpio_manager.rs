use std::fs::{OpenOptions, read_to_string};
use std::io::{Error, Write};

pub struct GpioManager;

const GPIO_BASE_PATH: &str = "/sys/class/gpio";
const GPIO_EXPORT_FILE: &str = "export";
const GPIO_DIRECTION_FILE: &str = "direction";
const GPIO_VALUE_FILE: &str = "value";

impl GpioManager {
    pub fn new() -> Self {
        Self {}
    }

    pub fn gpio_toggle(&self, pin_number: i32) -> bool {
        match self.init_pin(pin_number) {
            Ok(_) => {
                match self.set_pin_value(pin_number, !self.get_pin_value(pin_number)) {
                    Err(_error) => {
                        #[cfg(debug_assertions)]
                        println!("Reason: {:?}", _error);
                    }
                    _ => {}
                }
            }
            Err(_error) => {
                #[cfg(debug_assertions)]
                println!("Reason: {:?}", _error);
            }
        }

        self.get_pin_value(pin_number)
    }

    pub fn gpio_on(&self, pin_number: i32) -> bool {
        match self.init_pin(pin_number) {
            Ok(_) => {
                match self.set_pin_value(pin_number, true) {
                    Err(_error) => {
                        #[cfg(debug_assertions)]
                        println!("Reason: {:?}", _error);
                    }
                    _ => {}
                }
            }
            Err(_error) => {
                #[cfg(debug_assertions)]
                println!("Reason: {:?}", _error);
            }
        }

        self.get_pin_value(pin_number)
    }

    pub fn gpio_off(&self, pin_number: i32) -> bool {
        match self.init_pin(pin_number) {
            Ok(_) => {
                match self.set_pin_value(pin_number, false) {
                    Err(_error) => {
                        #[cfg(debug_assertions)]
                        println!("Reason: {:?}", _error);
                    }
                    _ => {}
                }
            }
            Err(_error) => {
                #[cfg(debug_assertions)]
                println!("Reason: {:?}", _error);
            }
        }

        self.get_pin_value(pin_number)
    }

    fn init_pin(&self, pin_number: i32) -> Result<(), Error> {
        self.export_pin(pin_number);
        self.set_pin_direction(pin_number, &String::from("out"))
    }

    fn export_pin(&self, pin_number: i32) {
        let mut file = match OpenOptions::new().read(false).write(true).open(format!("{}/{}", GPIO_BASE_PATH, GPIO_EXPORT_FILE)) {
            Err(error) => {
                println!("{:?}", error);

                return;
            }
            Ok(file) => file,
        };

        match file.write_all(pin_number.to_string().as_bytes()) {
            Err(_error) => {
                println!("Could not export pin {} \n(This is expected if pin is already exported)", pin_number);

                #[cfg(debug_assertions)]
                println!("Reason: {:?}", _error);
            }
            _ => {}
        }
    }

    fn set_pin_value(&self, pin_number: i32, state: bool) -> Result<(), Error> {
        self.write_to_gpio_file(
            &GPIO_VALUE_FILE.to_string(),
            pin_number,
            &(state as i32).to_string(),
        )
    }

    fn get_pin_value(&self, pin_number: i32) -> bool {
        let value = match read_to_string(format!(
            "{}/gpio{}/{}",
            GPIO_BASE_PATH,
            pin_number,
            GPIO_VALUE_FILE
        )) {
            Ok(value) => String::from(value.trim_matches('\n').trim_matches('\r')),
            Err(_error) => {
                println!("Could not read pin value");

                #[cfg(debug_assertions)]
                println!("Reason: {:?}", _error);

                String::new()
            }
        };

        return value == "1";
    }

    fn get_pin_direction(&self, pin_number: i32) -> String {
        let direction = match read_to_string(format!(
            "{}/gpio{}/{}",
            GPIO_BASE_PATH,
            pin_number,
            GPIO_DIRECTION_FILE
        )) {
            Ok(direction) => String::from(direction.trim_matches('\n').trim_matches('\r')),
            Err(_error) => {
                println!("Could not read pin direction");

                #[cfg(debug_assertions)]
                println!("Reason: {:?}", _error);

                String::new()
            }
        };

        return direction;
    }

    fn set_pin_direction(&self, pin_number: i32, direction: &String) -> Result<(), Error> {
        if &self.get_pin_direction(pin_number) != direction {
            return self.write_to_gpio_file(&GPIO_DIRECTION_FILE.to_string(), pin_number, direction);
        }

        Ok(())
    }

    fn write_to_gpio_file(&self, name: &String, pin_number: i32, value: &String) -> Result<(), Error> {
        let mut file = match OpenOptions::new().read(false).write(true).open(format!(
            "{}/gpio{}/{}",
            GPIO_BASE_PATH,
            pin_number,
            name
        )) {
            Err(error) => {
                println!("{:?}", &error);

                return Err(error);
            }
            Ok(file) => file,
        };

        match file.write_all(value.as_bytes()) {
            Err(error) => {
                println!("Could not write {}: {} on {}", name, value, pin_number);

                return Err(error);
            }
            _ => Ok(())
        }
    }
}
