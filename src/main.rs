extern crate new_home_application;
#[macro_use]
extern crate serde_json;
extern crate url;

use std::sync::{Arc, RwLock};

use new_home_application::application::app_error::AppError;
use new_home_application::application::application::ApplicationInfo;
use new_home_application::application::application_framework::{
    Application, ApplicationFramework, BootApplication,
};
use new_home_application::method::method_manager::MethodManager;
use new_home_application::method::method_structure::{Method, MethodArgument};

use crate::methods::device_action::DeviceAction;
use crate::methods::get_script::GetScript;
use crate::service::gpio_manager::GpioManager;
use crate::methods::config_root::ConfigRoot;

pub mod methods;
pub mod service;

struct GpioApplication {
    gpio_manager: Arc<RwLock<GpioManager>>,
}

impl GpioApplication {
    pub fn new(gpio_manager: GpioManager) -> Self {
        Self {
            gpio_manager: Arc::new(RwLock::new(gpio_manager)),
        }
    }
}

impl ApplicationFramework for GpioApplication {
    fn get_application_port(&self) -> i16 {
        4231
    }

    fn get_application_ip(&self) -> String {
        String::from("[::]")
    }

    fn get_application_info(&self) -> ApplicationInfo {
        ApplicationInfo {
            name: "gpio-application".to_string(),
            description: "Toggles on and of GPIO pins on a Raspberry PI".to_string(),
            authors: "Yannik_Sc".to_string(),
            version: "1.0.1".to_string(),
        }
    }

    fn setup_method_manager(&self, method_manager: &mut Box<dyn MethodManager>) {
        method_manager.add_method(
            Method {
                name: "device_action".to_string(),
                description: "Toggles a GPIO pin".to_string(),
                help: String::new(),
                arguments: vec![MethodArgument {
                    name: "channel".to_string(),
                    description: "The GPIO URL".to_string(),
                    help: "Format: gpio://<PIN_NUMBER>/<on|off|toggle".to_string(),
                    value: None,
                    required: true,
                }],
            },
            Box::new(DeviceAction::new(self.gpio_manager.clone())),
        );

        method_manager.add_method(
            Method {
                name: "get_script".to_string(),
                description: "Gets a script file from the resources directory".to_string(),
                help: String::new(),
                arguments: vec![MethodArgument {
                    name: "script_name".to_string(),
                    description: "The path to the script, that should be provided".to_string(),
                    help: String::new(),
                    value: None,
                    required: true,
                }],
            },
            Box::new(GetScript::new()),
        );

        method_manager.add_method(
            Method {
                name: "config_root".to_string(),
                description: "Gets a config view, which contains a channel builder".to_string(),
                help: String::new(),
                arguments: vec![],
            },
            Box::new(ConfigRoot::new()),
        );
    }
}

fn main() -> Result<(), AppError> {
    let mut app = Application::new(Box::new(GpioApplication::new(GpioManager::new())));
    app.boot()?;
    app.multi_run(4)?;

    Ok(())
}
